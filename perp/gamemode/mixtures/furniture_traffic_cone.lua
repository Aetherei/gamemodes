

local MIXTURE = {}

MIXTURE.ID = 90;
                   
MIXTURE.Results = "furniture_traffic_cone";
MIXTURE.Ingredients = {'item_chunk_plastic','item_chunk_plastic','item_chunk_plastic',};
MIXTURE.Requires = 	{

					};
					
MIXTURE.Free = true;

MIXTURE.RequiresHeatSource = false;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	return true;
end

GM:RegisterMixture(MIXTURE);