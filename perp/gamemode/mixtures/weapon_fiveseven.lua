


local MIXTURE = {}

MIXTURE.ID = 11;

MIXTURE.Results = "weapon_fiveseven";
MIXTURE.Ingredients = {'item_chunk_metal', 'item_chunk_metal', 'item_paint', 'item_paint', 'item_chunk_plastic'};
MIXTURE.Requires = 	{

					};
					
MIXTURE.Free = false;

MIXTURE.RequiresHeatSource = false;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	player:UnlockMixture(5);

	return true;
end

GM:RegisterMixture(MIXTURE);