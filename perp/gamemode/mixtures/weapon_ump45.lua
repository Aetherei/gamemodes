


local MIXTURE = {}

MIXTURE.ID = 61;

MIXTURE.Results = "weapon_ak47";
MIXTURE.Ingredients = {'item_chunk_metal', 'item_chunk_metal', 'item_chunk_metal', 'item_chunk_metal', 'item_metal_polish', 'item_board', 'item_board'};
MIXTURE.Requires = 	{

					};
					
MIXTURE.Free = false;

MIXTURE.RequiresHeatSource = false;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	player:UnlockMixture(6);

	return true;
end

GM:RegisterMixture(MIXTURE);