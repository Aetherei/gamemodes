


local MIXTURE = {}

MIXTURE.ID = 15;

MIXTURE.Results = "weapon_lock_pick";
MIXTURE.Ingredients = {'item_chunk_metal', 'item_metal_rod', 'item_wrench'};
MIXTURE.Requires = 	{

					};
					
MIXTURE.Free = true;

MIXTURE.RequiresHeatSource = true;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	return true;
end

GM:RegisterMixture(MIXTURE);