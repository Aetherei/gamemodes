


local MIXTURE = {}

MIXTURE.ID = 50;

MIXTURE.Results = "weapon_smoke_grenade";
MIXTURE.Ingredients = {'weapon_molotov', 'item_chunk_metal'};
MIXTURE.Requires = 	{

					};
					
MIXTURE.Free = true;

MIXTURE.RequiresHeatSource = false;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	return true;
end

GM:RegisterMixture(MIXTURE);