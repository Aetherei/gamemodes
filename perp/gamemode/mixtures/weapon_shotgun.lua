

local MIXTURE = {}

MIXTURE.ID = 9;

MIXTURE.Results = "fas2_rem870";
MIXTURE.Ingredients = {'item_chunk_metal', 'item_chunk_metal', 'item_chunk_metal', 'item_chunk_metal', 'item_metal_rod','item_metal_polish'};
MIXTURE.Requires = 	{

					};
					
MIXTURE.Free = true;

MIXTURE.RequiresHeatSource = false;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	player:UnlockMixture(7);

	return true;
end

GM:RegisterMixture(MIXTURE);