


local MIXTURE = {}

MIXTURE.ID = 8;

MIXTURE.Results = "weapon_uzi";
MIXTURE.Ingredients = {'item_chunk_metal', 'item_chunk_metal', 'item_chunk_metal', 'item_chunk_metal', 'item_chunk_plastic', 'item_metal_rod', 'item_paint'};
MIXTURE.Requires = 	{

					};
					
MIXTURE.Free = false;

MIXTURE.RequiresHeatSource = false;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	player:UnlockMixture(6);

	return true;
end

GM:RegisterMixture(MIXTURE);