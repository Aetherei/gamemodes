


local MIXTURE = {}

MIXTURE.ID = 4;

MIXTURE.Results = "fas2_ak47";
MIXTURE.Ingredients = {'item_chunk_metal', 'item_chunk_metal', 'item_chunk_metal', 'item_chunk_metal', 'item_chunk_metal', 'item_board', 'item_board', 'item_paint'};
MIXTURE.Requires = 	{
						
					};
					
MIXTURE.Free = true

MIXTURE.RequiresHeatSource = false;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	player:UnlockMixture(6);

	return true;
end

GM:RegisterMixture(MIXTURE);