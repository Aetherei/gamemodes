

local MIXTURE = {}

MIXTURE.ID = 5;

MIXTURE.Results = "ammo_pistol";
MIXTURE.Ingredients = {'item_bullet_shell', 'item_chunk_metal', 'item_cardboard'};
MIXTURE.Requires = 	{

					};
					
MIXTURE.Free = true;

MIXTURE.RequiresHeatSource = false;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	return true;
end

GM:RegisterMixture(MIXTURE);