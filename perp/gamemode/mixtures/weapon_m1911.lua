


local MIXTURE = {}

MIXTURE.ID = 60;

MIXTURE.Results = "fas2_m1911";
MIXTURE.Ingredients = {'item_chunk_metal', 'item_chunk_metal', 'item_metal_polish', 'item_metal_polish'};
MIXTURE.Requires = 	{

					};
					
MIXTURE.Free = true;

MIXTURE.RequiresHeatSource = false;
MIXTURE.RequiresWaterSource = false;
MIXTURE.RequiresSawHorse = false;

function MIXTURE.CanMix ( player, pos )
	player:UnlockMixture(5);

	return true;
end

GM:RegisterMixture(MIXTURE);