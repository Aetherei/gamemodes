


PROPERTY = {};

PROPERTY.ID = 39;

PROPERTY.Name = "Suburbs House #1";
PROPERTY.Category = "House";
PROPERTY.Description = "A moderately sized house in the suburbs.";
PROPERTY.Image = "ev3x_subs1";

PROPERTY.Cost = 3500;

PROPERTY.Doors = 	{
						{Vector(2590, 11877, 111), '*154'},
						{Vector(2588, 11480, 176.25), 'models/props_c17/door01_left.mdl'},
						{Vector(2537, 11724, 113), '*147'},
						{Vector(2382, 11604, 112.25), 'models/props_c17/door01_left.mdl'},
						{Vector(2084, 11610, 112.25), 'models/props_c17/door01_left.mdl'},
						{Vector(2276, 11824, 240.25), 'models/props_c17/door01_left.mdl'},
						{Vector(2388, 11808, 240.25), 'models/props_c17/door01_left.mdl'},	
						{Vector(2085, 11194, 241),'*212'},
					};
					
GAMEMODE:RegisterProperty(PROPERTY);