

PROPERTY = {};

PROPERTY.ID = 67;

PROPERTY.Name = "Industrial Warehouse #1";
PROPERTY.Category = "Business";
PROPERTY.Description = "A moderately sized werehouse next to the impound lot";
PROPERTY.Image = "industrial_warehouse_1";

PROPERTY.Cost = 1000;

PROPERTY.Doors = 	{

					{Vector(1489, 3587, 122), 'models/props_c17/door01_left.mdl'},
					{Vector(1199, 3877, 122), 'models/props_c17/door01_left.mdl'},
					{Vector(1832, 3941, 130), '*168'},
					{Vector(1832, 4096, 130), '*167'},
					{Vector(1832, 4251, 130), '*169'},

                    };
					
GAMEMODE:RegisterProperty(PROPERTY);