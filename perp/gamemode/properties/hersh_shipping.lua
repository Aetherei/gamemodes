


PROPERTY = {};

PROPERTY.ID = 65;

PROPERTY.Name = "Hersh Nightclub";
PROPERTY.Category = "Business";
PROPERTY.Description = "A Large nightclub.";
PROPERTY.Image = "ev3x_hershshippingco";

PROPERTY.Cost = 3500;

PROPERTY.Doors = 	{
						{Vector(4198, -8160, 158), 'models/props_c17/door01_left.mdl'},
						{Vector(4397, -8194, 174.5), '*74'},
						{Vector(4507, -8194, 174.5), '*73'},
						{Vector(5913, -7418, 158),'models/props_c17/door01_left.mdl'},
						{Vector(6261, -7418, 158),'models/props_c17/door01_left.mdl'},
						{Vector(6609, -7418, 158),'models/props_c17/door01_left.mdl'},
						{Vector(5923, -7688, 157), '*72'},
						{Vector(6271, -7688, 157), '*75'},
						{Vector(6619, -7688, 157), '*76'},
						{Vector(5575, -8608, 192),'*210'},
						{Vector(5575, -8352, 192),'*209'},
								
						--{Vector(5266, -8194, 174.5), '*155'},
						--{Vector(5376, -8194, 174.5), '*154'},

					};
					
GAMEMODE:RegisterProperty(PROPERTY);