


PROPERTY = {};

PROPERTY.ID = 117;

PROPERTY.Name = "Izzy's Palace";
PROPERTY.Category = "Business";
PROPERTY.Description = "A big party club.";
PROPERTY.Image = "ev33x_izzyspalace";

PROPERTY.Cost = 18000;

PROPERTY.Doors = 	{
						{Vector(-6967, -12643, 135), '*84'},
						{Vector(-7089, -12643, 135), '*85'},
						{Vector(-7279, -14379, 135), '*88'},
						{Vector(-7401, -14379, 135), '*89'},
						{Vector(-7408, -14314, 135), '*90'},
					};
					
GAMEMODE:RegisterProperty(PROPERTY);
