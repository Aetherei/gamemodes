

PROPERTY = {};

PROPERTY.ID = 33;

PROPERTY.Name = "MTL HQ";
PROPERTY.Category = "Business";
PROPERTY.Description = "A large complex in the industrial region.";
PROPERTY.Image = "ev33x_mtl_hq";

PROPERTY.Cost = 8000;

PROPERTY.Doors = 	{

						{Vector(-1242, 2786, 150), '*183'},					
						{Vector(-837, 2877, 150), 'models/props_c17/door01_left.mdl'},						
						{Vector(-1242, 2991, 158), '*164'},						
						{Vector(-1242, 3247, 158), '*162'},						
						{Vector(-1242, 3503, 158), '*98'},						
						{Vector(-1242, 3759, 158), '*97'},						
						{Vector(-1242, 4015, 158), '*99'},						
						{Vector(-1242, 4271, 158), '*103'},							
						{Vector(-1261, 5067, 126), 'models/props_c17/door01_left.mdl'},
						{Vector(-1552, 6268, 190), '*119'},						
						{Vector(-1936, 6268, 190), '*118'},						
						{Vector(-1936, 7062, 190), '*121'},						
						{Vector(-1552, 7062, 190), '*120'},						
						{Vector(-2582, 6193, 118), '*108'},					
						{Vector(-3173, 7189, 127), '*106'},						
						{Vector(-3051, 7189, 127), '*105'},
						{Vector(-2972, 5825, 118), '*107'},						
						{Vector(-2972, 5505, 118), '*109'},						
						{Vector(-2972, 5085, 118), '*110'},						
						{Vector(-2566, 8425, 118), '*114'},						
						{Vector(-2076, 8338, 156), '*112'},						
						{Vector(-1788, 8338, 156), '*113'},						
						{Vector(-1500, 8338, 156), '*156'},						
						{Vector(-2254, 8600, 118), 'models/props_warehouse/door.mdl'},
					
					};
					
GAMEMODE:RegisterProperty(PROPERTY);