

PROPERTY = {};

PROPERTY.ID = 68;

PROPERTY.Name = "Industrial Warehouse #2";
PROPERTY.Category = "Business";
PROPERTY.Description = "A moderately sized werehouse in the industrial estate";
PROPERTY.Image = "industrial_warehouse_2";

PROPERTY.Cost = 1000;

PROPERTY.Doors = 	{

					{Vector(2883, 3589, 122), 'models/props_c17/door01_left.mdl'},
					{Vector(3135, 3771, 122), 'models/props_c17/door01_left.mdl'},
					{Vector(3440, 3771, 130), '*166'},


                    };
					
GAMEMODE:RegisterProperty(PROPERTY);