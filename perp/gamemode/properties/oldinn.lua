
PROPERTY = {};

PROPERTY.ID = 36;

PROPERTY.Name = "Old Inn";
PROPERTY.Category = "House";
PROPERTY.Description = "A slightly old home that needs some work.";
PROPERTY.Image = "ev3x_oldinn";

PROPERTY.Cost = 750;

PROPERTY.Doors = 	{

						{Vector(-547, -8015, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-723, -8015, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-668, -8192, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1019, -8015, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1020, -8192, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1124, -7983, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1317, -7927, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1124, -7702, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1317, -7703, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1124, -7575, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1317, -7519, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1124, -7294, 115),'models/props_c17/door01_left.mdl'},
						{Vector(-1317, -7295, 115),'models/props_c17/door01_left.mdl'},




					};
					
GAMEMODE:RegisterProperty(PROPERTY);