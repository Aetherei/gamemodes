

local ITEM 					= {};

ITEM.ID 					= 28;
ITEM.Reference 				= "fas2_ak47";

ITEM.Name 					= "AK-47";
ITEM.Description			= "A fully-automatic beast.\n\nRequires Rifle Ammo.";

ITEM.Weight 				= 25;
ITEM.Cost					= 2400;

ITEM.MaxStack 				= 100;

ITEM.InventoryModel 		= "models/weapons/w_rif_ak47.mdl";
ITEM.ModelCamPos 				= Vector(15, 28, 0);
ITEM.ModelLookAt 				= Vector(12, 21, 1);
ITEM.ModelFOV 					= 60;
ITEM.WorldModel 			= "models/weapons/w_rif_ak47.mdl";

ITEM.RestrictedSelling	 	= false; // Used for drugs and the like. So we can't sell it.

ITEM.EquipZone 				= EQUIP_MAIN;											
ITEM.PredictUseDrop			= true; // If this isn't true, the server will tell the client when something happens to us based on the server's OnUse

if SERVER then

	function ITEM.OnUse ( Player )		
		return false;
	end
	
	function ITEM.OnDrop ( Player )
		return true;
	end
	
	function ITEM.Equip ( Player )
		Player:Give("fas2_ak47");
	end
	
	function ITEM.Holster ( Player )
		Player:StripWeapon("fas2_ak47");
	end
	
else

	function ITEM.OnUse ( slotID )	
		GAMEMODE.AttemptToEquip(slotID);
		
		return false;
	end
	
	function ITEM.OnDrop ( )
		return true;
	end
	
end

GM:RegisterItem(ITEM);