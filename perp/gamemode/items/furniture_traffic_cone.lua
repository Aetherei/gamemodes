


local ITEM 					= {};

ITEM.ID 					= 90;
ITEM.Reference 				= "furniture_traffic_cone";

ITEM.Name 					= "Traffic Cone";
ITEM.Description			= "Useful for guiding traffic.";

ITEM.Weight 				= 80;
ITEM.Cost					= 600;

ITEM.MaxStack 				= 100;

ITEM.InventoryModel 		= "models/props_junk/TrafficCone001a.mdl";
ITEM.ModelCamPos 				= Vector(36, 0, 5);
ITEM.ModelLookAt 				= Vector(0, 0, -3);
ITEM.ModelFOV 					= 70;
ITEM.WorldModel 			= "models/props_junk/TrafficCone001a.mdl";

ITEM.RestrictedSelling	 	= false; // Used for drugs and the like. So we can't sell it.

ITEM.EquipZone 				= nil;											
ITEM.PredictUseDrop			= false; // If this isn't true, the server will tell the client when something happens to us.

if SERVER then

	function ITEM.OnUse ( Player )
		local prop = Player:SpawnProp(ITEM);
		
		if (!prop || !IsValid(prop)) then return false; end
				
		return true;
	end
	
	function ITEM.OnDrop ( Player )
		return true;
	end
	
	function ITEM.Equip ( Player )

	end
	
	function ITEM.Holster ( Player )

	end
	
else

	function ITEM.OnUse ( slotID )		
		return true;
	end
	
	function ITEM.OnDrop ( )
		return true;
	end
	
end

GM:RegisterItem(ITEM);